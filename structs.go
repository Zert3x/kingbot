package main

import (
	"time"
)

type LimitationFragment struct {
	userID string
	usedAt time.Time
}
