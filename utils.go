package main

import (
	"crypto/hmac"
	"crypto/sha512"
	"encoding/hex"
	"fmt"
	"github.com/bwmarrin/discordgo"
	"math/rand"
	"time"
)

type Field struct {
	name string
	value string
}

func SendError(s *discordgo.Session, m* discordgo.MessageCreate, message string) {
	emb := NewEmbed().
		SetTitle("ERROR!").
		SetDescription(message).
		SetColor(0xff0000)
	s.ChannelMessageSendEmbed(m.ChannelID, emb.MessageEmbed)
}

func ArrayContains(arr []string, str string) bool {
	for _, k := range arr {
		if k == str {
			return true
		}
	}
	return false
}

func ArrayReverse(a []string) []string {
	b := a
	for i := len(b)/2-1; i >= 0; i-- {
		opp := len(b)-1-i
		b[i], b[opp] = b[opp], b[i]
	}
	return b
}

func UserHasRole(s *discordgo.Session, m *discordgo.MessageCreate, roleIDInput string) bool {
	member, err := s.State.Member(m.GuildID, m.Author.ID)
	if err != nil {
		if member, err = s.GuildMember(m.GuildID, m.Author.ID); err != nil {
			SendError(s,m, "errr.")
			return false
		}
	}
	for _, roleID := range member.Roles {
		if roleID == roleIDInput {
			return true
		}
	}
	return false
}

func ComputeHmac256(message []byte, secret []byte) string {
	hash := hmac.New(sha512.New, secret)
	hash.Write(message)

	// to lowercase hexits
	return hex.EncodeToString(hash.Sum(nil))

	//return base64.StdEncoding.EncodeToString(hash.Sum(nil))
}

func CheckLimitation(s *discordgo.Session, m *discordgo.MessageCreate, cmd *Command) (bool, string) {
	limit := cmd.limit


	if limit == -1 {
		return true, ""
	}
	var z int

	for _,l := range BotObject.Limits {
		if l.userID == m.Author.ID {
			z++
		}
	}
	now := time.Now().Local()

	if z >= limit {
		for v, k := range BotObject.Limits {
			if k.userID != m.Author.ID {
				continue
			}
			tm := k.usedAt
			a := time.Since(tm)
			if a.Hours() > 24 {
				fmt.Println("removing one")
				BotObject.Limits = append(BotObject.Limits[:v], BotObject.Limits[v+1:]...)
			} else {
				nnew := tm.AddDate(0,0,1)
				timeleft := nnew.Sub(now)
				return false, fmt.Sprintf("Cooldown: %s", timeleft.String())
			}
		}
	}
	tmp := LimitationFragment{userID: m.Author.ID, usedAt: now}
	BotObject.Limits = append(BotObject.Limits, tmp)
	return true, ""
}


func SelfDestructingMessage(s *discordgo.Session, m *discordgo.MessageCreate, title string, message string, secondsToDeath int) {
	emb := NewEmbed().SetTitle(title).SetDescription(message).SetColor(0xff9333)
	err := s.ChannelMessageDelete(m.ChannelID, m.ID)
	if err != nil {
		fmt.Println(err.Error())
	}
	msg, err := s.ChannelMessageSendEmbed(m.ChannelID, emb.MessageEmbed)
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	for i:=secondsToDeath; i>0; i-- {
		emb.SetFooter(fmt.Sprintf("Seconds to self destruct: %d", i))
		msg, _ = s.ChannelMessageEditEmbed(msg.ChannelID, msg.ID, emb.MessageEmbed)
		delaySecond(1)
	}
	s.ChannelMessageDelete(msg.ChannelID, msg.ID)
}

func delaySecond(n time.Duration) {
	time.Sleep(n * time.Second)
}

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
const (
	letterIdxBits = 6                    // 6 bits to represent a letter index
	letterIdxMask = 1<<letterIdxBits - 1 // All 1-bits, as many as letterIdxBits
	letterIdxMax  = 63 / letterIdxBits   // # of letter indices fitting in 63 bits
)

func RandStringBytesMaskImprSrc(n int) string {
	src := rand.NewSource(time.Now().UnixNano())
	b := make([]byte, n)
	// A src.Int63() generates 63 random bits, enough for letterIdxMax characters!
	for i, cache, remain := n-1, src.Int63(), letterIdxMax; i >= 0; {
		if remain == 0 {
			cache, remain = src.Int63(), letterIdxMax
		}
		if idx := int(cache & letterIdxMask); idx < len(letterBytes) {
			b[i] = letterBytes[idx]
			i--
		}
		cache >>= letterIdxBits
		remain--
	}

	return string(b)
}

func FooterTimestamp() string {
	return fmt.Sprintf("%s", time.Now().Format("Mon Jan 2 15:04:05"))
}
