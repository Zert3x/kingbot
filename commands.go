package main

import (
	"fmt"
	"github.com/bwmarrin/discordgo"
	"strconv"
	"strings"
)

type Command struct {
	ident string
	commands []string
	minArgs int
	roles []string
	desc string
	longHelp string
	limit int
	vipLimit int
	execute func(*discordgo.Session, *discordgo.MessageCreate, []string)
}

type CommandList struct {
	commands []*Command
}

func (list* CommandList) NewCommand(ident string, cmd []string, desc string, longHelp string, minArgs int, roles []string, limit int, vipLimit int, execute func(*discordgo.Session, *discordgo.MessageCreate, []string)) {
	tmp := &Command{ident: ident, commands:cmd, desc:desc, longHelp: longHelp, minArgs:minArgs, roles:roles, limit:limit, vipLimit:vipLimit, execute: execute}
	fmt.Println("Added command " + ident)
	list.commands = append(list.commands, tmp)
}

func (list* CommandList) Contains(cmd string) (*Command, bool) {
	for _, k := range list.commands {
		if k.contains(cmd) {
			return k, true
		}
	}
	return nil, false
}

func (c *Command) contains(a string) bool {
	for _, k := range c.commands {
		if k == a {
			return true
		}
	}
	return false
}

func (c *Command) canUse(s *discordgo.Session, m *discordgo.MessageCreate) (bool) {
	if len(c.roles) == 0 {
		return true
	}
	for _,k := range c.roles {
		if UserHasRole(s,m, k) {
			return true
		}
	}
	return false
}

func HelpCommand(s *discordgo.Session, m *discordgo.MessageCreate, args []string) {
	fmt.Println("help cmd")
	if len(args) > 0 {
		if cmd, ok := BotObject.CmdList.Contains(args[0]); ok {
			printHelpForCommand(s,m,cmd)
		} else {
			SendError(s,m,"Invalid command")
		}
	} else {
		emb := NewEmbed().
			SetTitle("Command Help").
			SetDescription("A list of commands this bot can handle").
			SetColor(0x00ff00)
		for _, k := range BotObject.CmdList.commands {
			emb.AddField(strings.Join(k.commands, " | "), k.desc)
		}

		_, err := s.ChannelMessageSendEmbed(m.ChannelID, emb.MessageEmbed)
		if err != nil {
			fmt.Println("ERR!\n" + err.Error())
		}
	}
}


func printHelpForCommand(s *discordgo.Session, m *discordgo.MessageCreate, cmd *Command) {
	emb := NewEmbed().
		SetTitle(cmd.ident).
		SetDescription(cmd.longHelp).
		SetColor(0x00ff00)
	_, err := s.ChannelMessageSendEmbed(m.ChannelID, emb.MessageEmbed)
	if err != nil {
		fmt.Println("ERR!\n" + err.Error())
	}
}

func PostToolCommand(s *discordgo.Session, m *discordgo.MessageCreate, args []string) {
	emb := NewEmbed().
		SetTitle(args[0]).
		SetThumbnail(BotObject.botUser.AvatarURL("250x250")).
		SetDescription(strings.Join(args[2:], " ")).
		AddField("Posted by", "<@"+m.Author.ID+">").
		AddField("Download", args[1]).
		SetColor(0xffaabb).
		SetFooter(FooterTimestamp())
	_, err := s.ChannelMessageSendEmbed("611311255650566145", emb.MessageEmbed)
	if err != nil {
		go SelfDestructingMessage(s, m, "Error", "Error posting tool", 5)
	}
	s.ChannelMessageDelete(m.ChannelID, m.ID)
}

func PingCommand(s *discordgo.Session, m *discordgo.MessageCreate, args []string) {
	emb := NewEmbed().
		SetTitle("Ping!").
		AddField("Bot Latency", s.HeartbeatLatency().String()).
		SetFooter(FooterTimestamp())
	msg, err := s.ChannelMessageSendEmbed(m.ChannelID, emb.MessageEmbed)
	if err != nil {
		go SelfDestructingMessage(s, m, "Error", "Error pinging", 5) // this shouldn't happen
		return
	}
	ours,err := msg.Timestamp.Parse()
	theirs, err := m.Timestamp.Parse()
	s.ChannelMessageDelete(m.ChannelID, m.ID)
	emb.AddField("Your Latency", (ours.Sub(theirs) - s.HeartbeatLatency()).String()).
		SetFooter(FooterTimestamp())
	s.ChannelMessageEditEmbed(msg.ChannelID, msg.ID, emb.MessageEmbed)
}

func KickCommand(s *discordgo.Session, m *discordgo.MessageCreate, args []string) {
	var desc string
	var id string
	if strings.Contains(args[0], "<@") {
		id = strings.TrimRight(strings.TrimLeft(args[0], "<@"), ">")
	} else {
		id = args[0]
	}
	user, err := s.GuildMember(m.GuildID, id)
	if err != nil {
		go SelfDestructingMessage(s,m, "Error", "Could not find user", 5)
		return
	}

	if len(args) == 1 {
		desc = "No reason specified"
	} else {
		desc = strings.Join(args[1:], " ")
	}
	c, err := s.UserChannelCreate(user.User.ID)
	if err == nil {
		_, err := s.ChannelMessageSend(c.ID, "You have been kicked for: " + desc)
		if err != nil {
			fmt.Println(err.Error())
		}
	}
	emb := NewEmbed().
		SetTitle("Kicking " + user.User.Mention()).
		SetDescription(desc).
		SetThumbnail(BotObject.botUser.AvatarURL("250x250")).
		SetFooter(FooterTimestamp()).
		SetColor(0xDE0202)

	err = s.GuildMemberDeleteWithReason(m.GuildID, user.User.ID, desc)
	if err != nil {
		go SelfDestructingMessage(s,m, "Error", "Could not kick " + user.Mention(), 5)
		return
	}

	_, err = s.ChannelMessageSendEmbed(m.ChannelID, emb.MessageEmbed)
	if err != nil {
		fmt.Println(err.Error())
	}
}

func BanCommand(s *discordgo.Session, m *discordgo.MessageCreate, args []string) {
	var desc string
	var id string
	if strings.Contains(args[0], "<@") {
		id = strings.TrimRight(strings.TrimLeft(args[0], "<@"), ">")
	} else {
		id = args[0]
	}
	user, err := s.GuildMember(m.GuildID, id)
	if err != nil {
		go SelfDestructingMessage(s,m, "Error", "Could not find user", 5)
		return
	}
	time := 9999

	if len(args) == 1 {
		a, err := strconv.Atoi(args[1])
		if err == nil {
			time = a
		}
		desc = "No reason specified"
	} else {
		desc = strings.Join(args[1:], " ")
	}
	c, err := s.UserChannelCreate(user.User.ID)
	if err == nil {
		_, err := s.ChannelMessageSend(c.ID, fmt.Sprintf("You have been banned for: %s\r\nYou will be able to join again in %d days", desc, time))
		if err != nil {
			fmt.Println(err.Error())
		}
	}
	emb := NewEmbed().
		SetTitle("Banning " + user.User.Mention()).
		SetDescription(desc).
		SetThumbnail(BotObject.botUser.AvatarURL("250x250")).
		SetFooter(FooterTimestamp()).
		SetColor(0xDE0202)

	err = s.GuildMemberDeleteWithReason(m.GuildID, user.User.ID, desc)
	err = s.GuildBanCreateWithReason(m.GuildID, user.User.ID, desc, time)
	if err != nil {
		go SelfDestructingMessage(s,m, "Error", "Could not kick " + user.Mention(), 5)
		return
	}

	_, err = s.ChannelMessageSendEmbed(m.ChannelID, emb.MessageEmbed)
	if err != nil {
		fmt.Println(err.Error())
	}
}

func PardonCommand(s *discordgo.Session, m *discordgo.MessageCreate, args []string) {
	var desc string
	var id string
	if strings.Contains(args[0], "<@") {
		id = strings.TrimRight(strings.TrimLeft(args[0], "<@"), ">")
	} else {
		id = args[0]
	}
	user, err := s.GuildMember(m.GuildID, id)
	if err != nil {
		go SelfDestructingMessage(s,m, "Error", "Could not find user", 5)
		return
	}

	emb := NewEmbed().
		SetTitle("Pardoning " + user.User.Mention()).
		SetDescription(desc).
		SetThumbnail(BotObject.botUser.AvatarURL("250x250")).
		SetFooter(FooterTimestamp()).
		SetColor(0x0700DC)

	err = s.GuildBanDelete(m.GuildID, id)
	if err != nil {
		go SelfDestructingMessage(s,m, "Error", "Could not kick " + user.Mention(), 5)
		return
	}

	_, err = s.ChannelMessageSendEmbed(m.ChannelID, emb.MessageEmbed)
	if err != nil {
		fmt.Println(err.Error())
	}
}