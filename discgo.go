// Declare this file to be part of the main package so it can be compiled into
// an executable.
package main

// Import all Go packages required for this file.
import (
	"fmt"
	"github.com/sclevine/agouti"
	"log"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"

	"github.com/bwmarrin/discordgo"
)

type IMVUBOT struct {
	Discord *discordgo.Session
	botUser         *discordgo.User
	commandPrefix string
	CmdList CommandList
	ChannelID	string
	allowedChannels []string
	supportChannel *discordgo.Channel
	Limits []LimitationFragment
	MainStatus string
	CrackerStatus string
	SDriver *agouti.WebDriver
}

var (
	BotObject *IMVUBOT
)

func init() {
	BotObject = new(IMVUBOT)

	/*
	Adding commands is super simple, just open commands.go, create a new function
	with this signature:
	func CommandFunction(s *discordgo.Session, m *discordgo.MessageCreate, args []string) { }

	and then add it with the function call below

	 */
	fmt.Println("Adding commands")
	//                           name of command     triggers to invoke    short description shown on ?help           longer description, shows on ?help cmd    minimum arguments    groups that can access   limits for free vs vip   Functoin object
	BotObject.CmdList.NewCommand("help", []string{"help", "?"}, "Shows this help message", "Shows a list of commands that this bot can handle", 0, []string{}, -1, -1, HelpCommand)
	BotObject.CmdList.NewCommand("tool", []string{"tool", "t"}, "Posts a tool", "Posts a tool to #cracking-tools-by-users\r\nUsage: tool {name} {link} {description{", 3, []string{}, -1, -1, PostToolCommand)
	BotObject.CmdList.NewCommand("ping", []string{"ping", "p"}, "Pings the bot", "Pings the bot and gets heartbeat latency", 0, []string{}, -1, -1, PingCommand)
	BotObject.CmdList.NewCommand("kick", []string{"kick", "k"}, "Kick a user with or without a reason.", "Kick a user with or without a reason\r\nUsage: kick {@user/id} [reason]", 1, []string{"610642625342734347", "610809498604666900"}, -1, -1, KickCommand)
	BotObject.CmdList.NewCommand("ban", []string{"ban", "b"}, "Ban a user with or without a reason.", "Ban a user with or without a reason\r\nUsage: ban {@user/id} [days] [reason]", 1, []string{"610642625342734347", "610809498604666900"}, -1, -1, BanCommand)
	BotObject.CmdList.NewCommand("pardon", []string{"pardon", "unban"}, "Pardon a banned user.", "Pardon a banned user\r\nUsage: pardon {userid}", 1, []string{"610642625342734347", "610809498604666900"}, -1, -1, PardonCommand)
}

func main() {
	BotObject.Discord, _ = discordgo.New("Bot ")
	/*if err != nil {
		fmt.Println("Error creating Discord session", err)
		return
	}*/
	user, err := BotObject.Discord.User("@me")
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	BotObject.botUser = user
	BotObject.commandPrefix = "?"
	BotObject.MainStatus = fmt.Sprintf("Kings With Methods")

	BotObject.Discord.AddHandler(commandHandler)
	BotObject.Discord.AddHandler(func(discord *discordgo.Session, ready *discordgo.Ready) {

		go func() {
			for {
				err = BotObject.Discord.UpdateStatus(0, BotObject.MainStatus)
				if err != nil {
					fmt.Println("Error attempting to set my status")
				}
				time.Sleep(25 * time.Second)
			}
		}()
		servers := BotObject.Discord.State.Guilds
		fmt.Println("Discord bot initialized, running on", len(servers), " servers")
		go func() {
			/*for _, guild := range BotObject.Discord.State.Guilds {
				fmt.Println(guild.Name)
				for _, channel := range guild.Channels {
					fmt.Println("-> " + channel.Name)
					if channel.Name == "support" {
						BotObject.supportChannel = channel
					}
				}
			}*/
		}()
	})

	// Open a websocket connection to Discord and begin listening.
	err = BotObject.Discord.Open()
	if err != nil {
		fmt.Println("error opening connection,", err)
		return
	}
	defer BotObject.Discord.Close()
	// Wait for a CTRL-C
	log.Printf(`Now running. Press CTRL-C to exit.`)
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc

}

func commandHandler(s *discordgo.Session, m *discordgo.MessageCreate) {

	// Ignore all messages created by the bot itself
	// This isn't required in this specific example but it's a good practice.
	if m.Author.ID == s.State.User.ID {
		return
	}

	cmd := m.Content

	// Check for command prefix
	if strings.HasPrefix(cmd, BotObject.commandPrefix) {
		cmd = strings.TrimPrefix(cmd, BotObject.commandPrefix)
		parts := strings.Split(cmd, " ")
		cmd = parts[0]
		args := parts[1:]
		if command, ok := BotObject.CmdList.Contains(cmd); ok {
			if  command.canUse(s, m) {
				if len(args) < command.minArgs {
					go SelfDestructingMessage(s,m, "Syntax Error", "Not enough arguments, see "+BotObject.commandPrefix+"help " + command.ident, 5)
				} else {
					canuse, msg := CheckLimitation(s,m,command)
					if canuse {
						go command.execute(s, m, args)
					} else {
						go SendError(s,m,msg)
					}
				}
			} else {
				go SelfDestructingMessage(s,m, "Permissions Error", "You don't have permission to use this command, sorry!", 5)
			}
		}

	}
}

